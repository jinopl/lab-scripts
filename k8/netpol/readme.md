Based on https://faun.pub/cka-exercises-network-policy-namespace-843bfead629e


check out this : https://orca.tufin.io/netpol/

Create all pods

```
cd k8/netpol/
kubectl create -f admin.yaml
kubectl create -f backend.yaml
kubectl create -f frontend.yaml
kubectl create -f test.yaml
```




```
kubectl exec -it web -n frontend -- curl api.backend | grep "Welcome to nginx"  && echo "===" && \
kubectl exec -it web -n frontend -- curl admin.admin | grep "Welcome to nginx"  && echo "===" && \
kubectl exec -it admin -n admin -- curl web.frontend | grep "Welcome to nginx"  && echo "===" && \
kubectl exec -it admin -n admin -- curl api.backend | grep "Welcome to nginx"  && echo "===" && \
kubectl exec -it api -n backend -- curl web.frontend | grep "Welcome to nginx"  && echo "===" && \
kubectl exec -it api -n backend -- curl admin.admin | grep "Welcome to nginx" 
```